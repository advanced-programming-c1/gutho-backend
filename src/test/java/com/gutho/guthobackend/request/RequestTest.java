package com.gutho.guthobackend.request;

//CHECKSTYLE:OFF
import static org.junit.jupiter.api.Assertions.*;
//CHECKSTYLE:ON

import com.gutho.guthobackend.core.request.Request;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RequestTest {
    private Request request;

    private static final String CHANNEL_ID = "TestChannelID";
    private static final String USER_ID = "TestUserID";
    private static final String MESSAGE = "Test Message";

    @BeforeEach
    public void setUp() {
        request = new Request(CHANNEL_ID, USER_ID, MESSAGE);
    }

    @Test
    public void getterTest() {
        assertEquals(CHANNEL_ID, request.getChannelID());
        assertEquals(USER_ID, request.getUserID());
        assertEquals(MESSAGE, request.getMessage());
    }
}
