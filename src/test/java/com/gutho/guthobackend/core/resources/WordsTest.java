package com.gutho.guthobackend.core.resources;

//CHECKSTYLE:OFF
import static org.junit.jupiter.api.Assertions.assertNotNull;
//CHECKSTYLE:ON

import org.junit.jupiter.api.Test;

public class WordsTest {
    private Words words;

    @Test
    public void randomWordTest() throws Exception {
        String randomString = Words.getInstance().randomWord();
        assertNotNull(randomString);
    }
}


