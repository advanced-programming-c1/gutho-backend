package com.gutho.guthobackend.core.states;

//CHECKSTYLE:OFF
import static org.junit.jupiter.api.Assertions.*;
//CHECKSTYLE:ON

import com.gutho.guthobackend.core.games.Hangmen;
import com.gutho.guthobackend.core.response.Response;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class HangmenTest {
    private Hangmen hangmen;
    private static final String CHANNEL_ID = "testChannelID";
    private static final String USER_ID = "testUserID";

    /**
     * Setup to run before every test.
     */
    @BeforeEach
    public void setUp() {
        hangmen = new Hangmen(CHANNEL_ID);
    }

    @Test
    public void handleMessageTest() throws Exception {
        Response response = hangmen.handleMessage(USER_ID, "dummyString");
        assertEquals("", response.getMessage());

        Response response2 = hangmen.handleMessage(USER_ID, "!join");
        assertEquals("You successfully joined the game! Type \"!start\" to start the game!",
                response2.getMessage());

        Response response3 = hangmen.handleMessage(USER_ID, "!join");
        assertEquals("You have already joined the game!", response3.getMessage());
    }

    @Test
    public void initialNextStateTest() throws Exception {
        hangmen.nextState();
    }

    @Test
    public void participantsGetterAndAdderTest() throws Exception {
        hangmen.addParticipant("user1");
        hangmen.addParticipant("user2");
        ArrayList<String> participants = hangmen.getParticipantsID();

        assertEquals("user1", participants.get(0));
        assertEquals("user2", participants.get(1));
    }

    @Test
    public void getGameIDTest() throws Exception {
        assertEquals(CHANNEL_ID, hangmen.getGameID());
    }

    @Test
    public void currentWordGetterSetterTest() throws Exception {
        hangmen.setCurrentWord("dummyWord");
        String currentWord = hangmen.getCurrentWord();

        assertEquals("dummyWord", currentWord);
    }

    @Test
    public void guessedCharGetterSetterTest() throws Exception {
        Boolean[] dummyArray = {true, false};

        hangmen.setGuessedChar(dummyArray);
        Boolean[] getArray = hangmen.getGuessedChar();

        assertTrue(getArray[0]);
        assertFalse(getArray[1]);
    }

    @Test
    public void getAndIncreaseRoundNo() throws Exception {
        hangmen.getAndIncreaseRoundNo();
    }
}
