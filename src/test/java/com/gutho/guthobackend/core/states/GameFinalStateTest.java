package com.gutho.guthobackend.core.states;

import com.gutho.guthobackend.core.games.Hangmen;
import com.gutho.guthobackend.core.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
//CHECKSTYLE:OFF
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
//CHECKSTYLE:ON

public class GameFinalStateTest {
    private GameFinalState gameFinalState;
    private Hangmen hangmen;
    //CHECKSTYLE:OFF
    private final String CHANNEL_ID = "ChannelDummy";


    @BeforeEach
    //CHECKSTYLE:ON
    public void setUp() throws Exception {
        hangmen = new Hangmen(CHANNEL_ID);
        hangmen.setParticipantsScore(new HashMap<String, Integer>() {
            {
                put("USER1", 4);
                put("USER2", 3);
            }
        });
        gameFinalState = new GameFinalState(hangmen);
    }

    @Test
    void testFinalOutputForFinalState() {
        Response finalresponse = gameFinalState.handleMessage("USER1", "");
        String expectedOutput = gameFinalState.listScoresString() + gameFinalState.getGameWinner();
        assertEquals(finalresponse.getMessage(), expectedOutput);
    }

    @Test
    void testListScoresString() {
        String expectedOutput = "FINAL SCORE:\n" + "<@USER1> : 4" + "\n" + "<@USER2> : 3" + "\n";
        assertEquals(expectedOutput, gameFinalState.listScoresString());
    }

    @Test
    void testGetGameWinner() {
        String expectedOutput = "The Winner of this Hangman game is <@USER1>";
        assertEquals(expectedOutput, gameFinalState.getGameWinner());
    }

    @Test
    void testNextState() {
        assertEquals(gameFinalState.nextState(), null);
    }
}
