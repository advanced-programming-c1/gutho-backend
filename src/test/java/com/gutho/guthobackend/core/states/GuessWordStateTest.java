package com.gutho.guthobackend.core.states;

import com.gutho.guthobackend.core.games.Hangmen;
import com.gutho.guthobackend.core.response.Response;
import java.util.HashMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
//CHECKSTYLE:OFF
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
//CHECKSTYLE:ON

public class GuessWordStateTest {
    private GuessWordState guessWord;
    private Hangmen hangmen;
    private HashMap<String,Integer> currentHp;
    //CHECKSTYLE:OFF
    private final String CHANNEL_ID = "9203";
    private final String USER_ID1 = "23";
    private final String USER_ID2 = "24";
    private final String USER_ID3 = "25";
    private final String USER_ID4 = "26";
    //CHECKSTYLE:ON

    @BeforeEach
    void setUp() {
        hangmen = new Hangmen(CHANNEL_ID);
        hangmen.addParticipant(USER_ID1);
        hangmen.addParticipant(USER_ID2);
        hangmen.addParticipant(USER_ID3);
        hangmen.setCurrentWord("banana");
        hangmen.setChoosenID(USER_ID1);
        hangmen.setParticipantsScore(new HashMap<String, Integer>() {
            {
                put(USER_ID1, 0);
                put(USER_ID2, 0);
                put(USER_ID3, 0);
            }
        });
        guessWord = new GuessWordState(hangmen);
    }

    @Test
    public void checkUserIdIsTheChoosenId() {
        Response response = guessWord.handleMessage(USER_ID1, "b");
        assertEquals("You chose the word, so you can't play.", response.getMessage());
    }

    @Test
    public void checkUserWithZeroHpCantPlay() {
        for (int i = 0; i < 7; i++) {
            guessWord.handleMessage(USER_ID3, "x").getMessage();
        }
        Response response = guessWord.handleMessage(USER_ID3, "b");
        assertEquals(
                "<@25>, your current HP = 0, you cannot guess on this round again :(",
                response.getMessage()
        );
    }

    @Test
    public void checkAllUserHaveZeroHp() {
        for (int i = 0; i < 6; i++) {
            guessWord.handleMessage(USER_ID3, "x");
            guessWord.handleMessage(USER_ID2, "x");
        }
        guessWord.handleMessage(USER_ID3, "x");
        Response response = guessWord.handleMessage(USER_ID2, "x");
        String expect  = "Nobody can guess the word! The word was \"banana\"\n"
                + " CURRENT SCORE:\n"
                + "<@23> : 0\n"
                + "<@24> : 0\n"
                + "<@25> : 0\n"
                + "Type \"!startRound\" to start the next round!";
        assertEquals(expect, response.getMessage());
    }

    @Test
    public void checkUserGuessCorrectly() {
        Response response = guessWord.handleMessage(USER_ID2, "b");
        String finalResponse = response.getMessage();
        assertEquals("b \\_ \\_ \\_ \\_ \\_ ", finalResponse);
    }

    @Test
    public void checkUserGuessWrongAndPrintCurrentHp() {
        Response response = guessWord.handleMessage(USER_ID2, "x");
        String expect = "Current HP: \n"
                + "<@23> : 0\n"
                + "<@24> : 6\n"
                + "<@25> : 7\n";
        String currentHpString = response.getMessage();
        assertEquals(expect, currentHpString);
    }

    @Test
    public void checkAllGuessWordAlreadyGuessAndAddScore() {
        guessWord.handleMessage(USER_ID2, "b");
        guessWord.handleMessage(USER_ID2, "n");
        Response response = guessWord.handleMessage(USER_ID2, "a");
        String expect = "The word has been guessed! The word was \"banana\"\n"
                + " CURRENT SCORE:\n" + "<@23> : 0\n"
                + "<@24> : 1\n" + "<@25> : 0\n"
                + "Type \"!startRound\" to start the next round!";
        assertEquals(1, hangmen.getParticipantsScore().get(USER_ID2));
        assertEquals(expect, response.getMessage());
    }

    @Test
    public void checkUserGuessWordThatAlreadyGuessed() {
        guessWord.handleMessage(USER_ID2, "b");
        Response response = guessWord.handleMessage(USER_ID3, "b");
        // It will return false and decrease USER_ID3 hp
        String expect = "Current HP: \n"
                + "<@23> : 0\n"
                + "<@24> : 7\n"
                + "<@25> : 6\n";
        assertEquals(expect, response.getMessage());
    }

    @Test
    public void checkNextStateChooseWordStateAndIncreaseRound() {
        State nextState = guessWord.nextState();
        assertTrue(nextState instanceof ChooseWordState);
        assertEquals(1, hangmen.getRoundNo());
    }

    @Test
    public void checkNextStateGameFinalState() {
        hangmen.setRoundNo(2);
        guessWord = new GuessWordState(hangmen);
        State nextState = guessWord.nextState();
        assertTrue(nextState instanceof GameFinalState);
    }


}
