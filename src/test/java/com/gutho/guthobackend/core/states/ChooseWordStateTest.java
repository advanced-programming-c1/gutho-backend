package com.gutho.guthobackend.core.states;

//CHECKSTYLE:OFF
import static org.junit.jupiter.api.Assertions.*;
//CHECKSTYLE:ON

import com.gutho.guthobackend.core.games.Hangmen;
import com.gutho.guthobackend.core.response.Response;
import com.gutho.guthobackend.service.GameService;
import java.util.HashMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class ChooseWordStateTest {
    private GameService gameService;
    private Hangmen hangmen;
    private ChooseWordState chooseWord;
    private static final String CHANNEL_ID = "testChannelID";
    private static final String USER_ID = "testUserID";

    /**
     * Setup to run before every test.
     */
    @BeforeEach
    public void setUp() {
        hangmen = new Hangmen(CHANNEL_ID);
        chooseWord = new ChooseWordState(hangmen);
        hangmen.addParticipant(USER_ID);

    }

    @Test
    public void  autoInputReturnNotNullResponse() throws Exception {
        Response response1 = chooseWord.handleMessage(USER_ID, "!startRound");
        assertEquals("Choose a word to be guessed...", response1.getMessage());

        Response response2 = chooseWord.handleMessage(USER_ID, "auto");
        assertNotNull(response2.getMessage());
    }

    @Test
    public void userNotInTurnShouldBeEmptyStringResponse() throws Exception {
        Response response1 = chooseWord.handleMessage(USER_ID, "!startRound");
        assertEquals("Choose a word to be guessed...", response1.getMessage());

        Response response2 = chooseWord.handleMessage("bukanUser", "dummyInput");
        assertEquals("", response2.getMessage());
    }

    @Test
    public void nextStateShouldBeGuessWordState() throws Exception {
        chooseWord.handleMessage(USER_ID, "!startRound");
        chooseWord.handleMessage(USER_ID, "apple");
        State nextState = chooseWord.nextState();
        assertTrue(nextState instanceof GuessWordState);
    }
}
