package com.gutho.guthobackend.core.states;

import com.gutho.guthobackend.core.games.Game;
import com.gutho.guthobackend.core.games.Hangmen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
//CHECKSTYLE:OFF
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
//CHECKSTYLE:ON

class RegistrationStateTest {
    private RegistrationState registrationStateObj;
    private Game game;
    //CHECKSTYLE:OFF
    private final String CHANNEL_ID = "9203";
    private final String USER_ID = "23";
    //CHECKSTYLE:ON

    @BeforeEach
    void setUp() throws Exception {
        game = new Hangmen(CHANNEL_ID);
        registrationStateObj = new RegistrationState(game);
    }

    @Test
    void testHandleMessageMustReturnSpecifiedResponseWhenSomeoneJoinAndStartGame() {
        assertEquals("You successfully joined the game! Type \"!start\" to start the game!",
                registrationStateObj.handleMessage(USER_ID, "!join").getMessage());
        assertEquals("More than 1 player needed to start the game!",
                registrationStateObj.handleMessage(USER_ID, "!start").getMessage());
    }

    @Test
    void testHandleMessageWhenSameUserIDStartMustReturnSpecifiedResponse() {
        registrationStateObj.handleMessage(USER_ID, "!join");
        assertEquals("You have already joined the game!",
                registrationStateObj.handleMessage(USER_ID, "!join").getMessage());
    }

    @Test
    void testHandleMessageWhenUserJoiningExistingGame() {
        assertEquals("You successfully joined the game! Type \"!start\" to start the game!",
                registrationStateObj.handleMessage("628", "!join").getMessage());
    }

    @Test
    void testHandleMessageWhenDifferentUserJoiningGame() {
        registrationStateObj.handleMessage("628", "!join");
        assertEquals("You successfully joined the game! Type \"!start\" to start the game!",
                registrationStateObj.handleMessage("629", "!join").getMessage());
    }

    @Test
    void testHandleMessageWhenMoreThanOnePlayerHasJoined() {
        registrationStateObj.handleMessage(USER_ID + "1", "!join");
        registrationStateObj.handleMessage(USER_ID + "2", "!join");
        assertEquals("Game will be started! Type \"!startRound\" to start the round!",
                registrationStateObj.handleMessage(USER_ID + "1", "!start").getMessage());
    }
}
