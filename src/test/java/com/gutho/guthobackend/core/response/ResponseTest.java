package com.gutho.guthobackend.core.response;

//CHECKSTYLE:OFF
import static org.junit.jupiter.api.Assertions.*;
//CHECKSTYLE:ON

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ResponseTest {
    private Response response;

    private static final String RESPONSE_TYPE = "PM";
    private static final String USER_ID = "TestUserID";
    private static final String MESSAGE = "Test Message";

    @BeforeEach
    public void setUp() {
        response = new Response(RESPONSE_TYPE, USER_ID, MESSAGE);
    }

    @Test
    public void getterTest() {
        assertEquals(RESPONSE_TYPE, response.getResponseType());
        assertEquals(USER_ID, response.getUserID());
        assertEquals(MESSAGE, response.getMessage());
    }
}
