package com.gutho.guthobackend.repository;

import com.gutho.guthobackend.core.games.Hangmen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.stereotype.Repository;

//CHECKSTYLE:OFF
import static org.junit.jupiter.api.Assertions.assertEquals;
//CHECKSTYLE:ON

@Repository
public class GameRepositoryTests {
    private GameRepository gameRepository;
    private Hangmen hangmen;
    private static final String CHANNEL_ID = "testChannelID";

    /**
     * Setup to run before every test.
     */
    @BeforeEach
    public void setUp() {
        gameRepository = new GameRepository();
        hangmen = new Hangmen(CHANNEL_ID);
        gameRepository.add(hangmen);
    }

    @Test
    public void getGameByIDShouldReturnGameWithTheSameID() throws Exception {
        assertEquals(hangmen, gameRepository.getGameById(CHANNEL_ID));
    }

    @Test
    public void whenNoGameMatchChannelIDGetGameByIdShouldReturnNull() throws Exception {
        assertEquals(null, gameRepository.getGameById("testInvalidChannelID"));
    }
}
