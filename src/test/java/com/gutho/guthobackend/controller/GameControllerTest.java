package com.gutho.guthobackend.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.gutho.guthobackend.core.request.Request;
import com.gutho.guthobackend.core.response.Response;
import com.gutho.guthobackend.service.GameServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

//CHECKSTYLE:OFF
import java.awt.*;

import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//CHECKSTYLE:ON

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WebMvcTest(controllers = GameController.class)
public class GameControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private GameServiceImpl gameService;

    private Request request;
    private Response response;

    private static final String CHANNEL_ID = "TestChannelID";
    private static final String USER_ID = "TestUserID";
    private static final String MESSAGE = "Test Message";
    private static final String RESPONSE_TYPE = "PM";

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return objectMapper.writeValueAsString(obj);
    }


    @BeforeEach
    public void setUp() {
        request = new Request(CHANNEL_ID, USER_ID, MESSAGE);
        response = new Response(RESPONSE_TYPE, USER_ID, MESSAGE);
    }

    @Test
    public void testProcessMessage() throws Exception {
        when(gameService.handleMessage(anyString(), anyString(), anyString())).thenReturn(response);

        String jsonBody = mapToJson(request);

        mvc.perform(post("/api/v1/processMessage/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody))
                .andExpect(status().isOk());
    }
}
