package com.gutho.guthobackend.service;

import com.gutho.guthobackend.core.games.Game;
import com.gutho.guthobackend.core.games.Hangmen;
import com.gutho.guthobackend.core.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

//CHECKSTYLE:OFF
import static org.junit.jupiter.api.Assertions.assertEquals;
//CHECKSTYLE:ON

public class GameServiceTest {
    private GameService gameService;
    private static final String CHANNEL_ID = "testChannelID";
    private static final String USER_ID = "testUserID";

    @BeforeEach
    public void setUp() {
        gameService = new GameServiceImpl();
    }

    @Test
    public void whenNewGameCreatedItsIDMustBeTheSameAsChannelID() throws Exception {
        Game createdGame = gameService.createGame(CHANNEL_ID);
        assertEquals(CHANNEL_ID, createdGame.getGameID());
    }

    @Test
    public void handleMessageTestWhenNoGameCreated() throws Exception {
        Response response = gameService.handleMessage(CHANNEL_ID, USER_ID, "test");
        assertEquals("", response.getMessage());
    }

    @Test
    public void handleMessageTestWhenCreateNewGameAndReregister() throws Exception {
        Response response = gameService.handleMessage(CHANNEL_ID, USER_ID, "!create");
        assertEquals("New game created! You successfully joined the game! Type \"!start\" "
                        + "to start the game!",
                response.getMessage());

        response = gameService.handleMessage(CHANNEL_ID, USER_ID, "!join");
        assertEquals("You have already joined the game!", response.getMessage());
    }

    @Test
    public void handleMessageTestWhenReceivingMessageFromPrivateMessage() throws Exception {
        Game createdGame = gameService.createGame(CHANNEL_ID);
        createdGame.addParticipant(USER_ID);
        Response response = gameService.handleMessage(USER_ID, USER_ID, "!join");
        assertEquals("You have already joined the game!", response.getMessage());
    }

    @Test
    public void gameShouldBeDeletedWhenStateIsGameFinalState() throws Exception {
        Hangmen createdGame = (Hangmen) gameService.createGame(CHANNEL_ID);
        createdGame.addParticipant(USER_ID);
        createdGame.nextState();
        createdGame.setCurrentWord("TEST");
        createdGame.nextState();
        createdGame.nextState();
        System.out.println(createdGame.getState());
        gameService.handleMessage(CHANNEL_ID, USER_ID, "!startRound");
        assertEquals("New game created! You successfully joined the game! Type \"!start\" "
                        + "to start the game!",
                gameService.handleMessage(CHANNEL_ID, USER_ID, "!create").getMessage());
    }
}
