package com.gutho.guthobackend.service;

import com.gutho.guthobackend.core.games.Game;
import com.gutho.guthobackend.core.response.Response;

public interface GameService {
    Game createGame(String channelID);

    Response handleMessage(String channelID, String userID, String message);
}
