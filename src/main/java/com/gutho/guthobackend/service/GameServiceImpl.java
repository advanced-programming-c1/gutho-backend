package com.gutho.guthobackend.service;

import com.gutho.guthobackend.core.games.Game;
import com.gutho.guthobackend.core.games.Hangmen;
import com.gutho.guthobackend.core.response.Response;
import com.gutho.guthobackend.core.states.GameFinalState;
import com.gutho.guthobackend.repository.GameRepository;
import org.springframework.stereotype.Service;

@Service
public class GameServiceImpl implements GameService {
    private GameRepository repo;

    public GameServiceImpl(GameRepository repo) {
        this.repo = repo;
    }

    public GameServiceImpl() {
        this(new GameRepository());
    }

    @Override
    public Game createGame(String channelID) {
        Game newGame = new Hangmen(channelID);

        return repo.add(newGame);
    }

    @Override
    public Response handleMessage(String channelID, String userID, String message) {
        Game game = repo.getGameById(channelID);

        if (game == null && !message.equals("!create")) {
            for (Game gameItr: repo.getList()) {
                for (String userIDItr: gameItr.getParticipantsID()) {
                    if (userID.equals(userIDItr)) {
                        Response response = gameItr.handleMessage(userID, message);
                        return response;
                    }
                }
            }
        }

        if (game == null && message.equals("!create")) {
            game = createGame(channelID);

            Response response = game.handleMessage(userID, "!join");
            response.setMessage("New game created! " + response.getMessage());

            return response;
        } else if (game != null) {
            Response response;
            if (game.getState() instanceof GameFinalState) {
                response = game.handleMessage(userID, message);
                repo.delete(game);
            } else {
                response = game.handleMessage(userID, message);
            }
            return response;
        }
        return new Response("GM", userID, "");
    }
}
