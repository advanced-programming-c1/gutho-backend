package com.gutho.guthobackend.core.request;

public class Request {
    String channelID;
    String userID;
    String message;

    /**
     * Constructor for the Request class.
     * @param channelID Origin channel ID of the message.
     * @param userID Author ID of the message.
     * @param message The content of the message.
     */
    public Request(String channelID, String userID, String message) {
        this.channelID = channelID;
        this.userID = userID;
        this.message = message;
    }

    public String getChannelID() {
        return channelID;
    }

    public String getUserID() {
        return userID;
    }

    public String getMessage() {
        return message;
    }
}