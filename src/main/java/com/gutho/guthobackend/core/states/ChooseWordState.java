package com.gutho.guthobackend.core.states;

import com.gutho.guthobackend.core.games.Game;
import com.gutho.guthobackend.core.games.Hangmen;
import com.gutho.guthobackend.core.resources.Words;
import com.gutho.guthobackend.core.response.Response;
import java.util.ArrayList;

public class ChooseWordState implements State {
    private Hangmen game;
    private Boolean hasChosenWord;
    private String currentChooserID;

    public ChooseWordState(Game game) {
        this.game = (Hangmen) game;
        this.hasChosenWord = false;
    }

    @Override
    public Response handleMessage(String userID, String message) {

        if (hasChosenWord == false && message.equals("!startRound")) {
            int currentRound = game.getRoundNo();
            ArrayList<String> participantsID = game.getParticipantsID();
            currentChooserID = participantsID.get(currentRound);
            game.setChoosenID(currentChooserID);
            hasChosenWord = true;
            return new Response("PM", currentChooserID, "Choose a word to be guessed...");
        } else {
            if (userID.equals(currentChooserID)) {
                String choosenString = message.split(" ")[0];
                if (choosenString.equalsIgnoreCase("auto")) {
                    choosenString = Words.getInstance().randomWord();
                }
                game.setCurrentWord(choosenString);
                String responseMessage = "";
                for (int i = 0; i < choosenString.length(); i++) {
                    responseMessage += "\\_ ";
                }
                game.nextState();
                return new Response("GM2", game.getGameID(), responseMessage);
            } else {
                return new Response("GM", userID, "");
            }
        }
    }

    @Override
    public State nextState() {
        return new GuessWordState(game);
    }
}
