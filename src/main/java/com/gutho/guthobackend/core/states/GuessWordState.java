package com.gutho.guthobackend.core.states;

import com.gutho.guthobackend.core.games.Game;
import com.gutho.guthobackend.core.games.Hangmen;
import com.gutho.guthobackend.core.response.Response;
import java.util.HashMap;
import java.util.Map;

public class GuessWordState implements State {
    private Hangmen game;
    private HashMap<String, Boolean> alreadyGuess = new HashMap<>();
    private String responseMessage;
    private String curWord;
    private HashMap<String, Integer> currentHp;
    private Boolean[] guessedChar;


    /**
     * GuessWord
     * Didalamnya membuat pencatatan untuk siapa saja yang sudah menebak kata selama round tersebut.
     * @param game game hangman
     */
    public GuessWordState(Game game) {
        this.game = (Hangmen) game;
        this.curWord = this.game.getCurrentWord();
        this.currentHp = new HashMap<>();
        for (String name : this.game.getParticipantsID()) {
            if (name.equals(this.game.getChoosenID())) {
                currentHp.put(name, 0);
            } else {
                currentHp.put(name, 7);
            }
        }
        this.guessedChar = new Boolean[curWord.length()];
        for (int i = 0; i < guessedChar.length; i++) {
            guessedChar[i] = false;
        }
        alreadyGuess.put(this.game.getChoosenID(), true);
    }

    @Override
    public Response handleMessage(String userID, String message) {
        String choosenID = game.getChoosenID();
        HashMap<String, Integer> currentScore = game.getParticipantsScore();
        if (userID.equals(choosenID)) {
            responseMessage = "You chose the word, so you can't play.";
            return new Response("GM", userID, responseMessage);
        } else if (currentHp.get(userID) == 0) {
            responseMessage = "<@" + userID + ">, your current HP = 0,"
                    + " you cannot guess on this round again :(";
            return new Response("GM", userID, responseMessage);
        } else if (
                curWord.indexOf(message.charAt(0)) != -1
                        && !guessedChar[curWord.indexOf(message.charAt(0))]
        ) {
            for (int i = 0; i < curWord.length(); i++) {
                if (curWord.charAt(i) == message.charAt(0)) {
                    guessedChar[i] = true;
                }
            }
            responseMessage = "";
            boolean allTrueFlag = true;
            for (int i = 0; i < curWord.length(); i++) {
                if (guessedChar[i] == false) {
                    responseMessage += "\\_ ";
                    allTrueFlag = false;
                } else {
                    responseMessage += curWord.charAt(i) + " ";
                }
            }
            if (allTrueFlag) {
                int currentParticipantScore = currentScore.get(userID);
                currentParticipantScore++;
                currentScore.put(userID, currentParticipantScore);
                game.setParticipantsScore(currentScore);
                responseMessage = "";
                for (Map.Entry<String, Integer> entry: currentScore.entrySet()) {
                    responseMessage += "<@" + entry.getKey()
                            + "> : " + entry.getValue().toString() + "\n";
                }
                game.nextState();
                return new Response(
                        "GM", userID,
                        "The word has been guessed! The word was \"" + curWord
                                + "\"\n CURRENT SCORE:\n" + responseMessage
                                + "Type \"!startRound\" to start the " + "next round!");
            } else {
                return new Response("GM", userID, responseMessage);
            }
        } else {
            int currentParticipantHp = currentHp.get(userID);
            currentParticipantHp--;
            currentHp.put(userID, currentParticipantHp);
            boolean allZeroHpFlag = true;
            for (Map.Entry<String, Integer> entry: currentHp.entrySet()) {
                if (entry.getValue() > 0) {
                    allZeroHpFlag = false;
                    break;
                }
            }
            if (allZeroHpFlag) {
                responseMessage = "";
                for (Map.Entry<String, Integer> entry: currentScore.entrySet()) {
                    responseMessage += "<@" + entry.getKey() + "> : "
                            + entry.getValue().toString() + "\n";
                }
                game.nextState();
                return new Response(
                        "GM", userID,
                        "Nobody can guess the word! The word was \"" + curWord
                                + "\"\n CURRENT SCORE:\n" + responseMessage
                                + "Type \"!startRound\" to start the " + "next round!");
            }
            responseMessage = "";
            for (Map.Entry<String, Integer> entry: currentHp.entrySet()) {
                responseMessage += "<@" + entry.getKey() + "> : "
                        + entry.getValue().toString() + "\n";
            }
            return new Response("GM", userID, "Current HP: \n" + responseMessage);
        }
    }

    @Override
    public State nextState() {
        if ((game.getAndIncreaseRoundNo() + 1) == game.getParticipantsID().size()) {
            return new GameFinalState(game);
        } else {
            return new ChooseWordState(game);
        }
    }

}
