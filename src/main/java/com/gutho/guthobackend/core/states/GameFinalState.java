package com.gutho.guthobackend.core.states;

import com.gutho.guthobackend.core.games.Game;
import com.gutho.guthobackend.core.games.Hangmen;
import com.gutho.guthobackend.core.response.Response;
import java.util.HashMap;
import java.util.Map;

public class GameFinalState implements State {
    private Hangmen game;

    public GameFinalState(Game game) {
        this.game = (Hangmen) game;
    }

    @Override
    public Response handleMessage(String userID, String message) {
        return new Response("GM", userID, listScoresString() + getGameWinner());
    }

    @Override
    public State nextState() {
        return null;
    }

    /**
     * Fungsi ini mengembalikan string berisikan skor akhir dari pemain.
     * @return String berisikan skor akhir dari pemain.
     */
    public String listScoresString() {
        HashMap<String, Integer> participantsScore = this.game.getParticipantsScore();
        String finalResponse = "FINAL SCORE:\n";
        for (Map.Entry<String, Integer> entry: participantsScore.entrySet()) {
            finalResponse += "<@" + entry.getKey() + "> : " + entry.getValue().toString() + "\n";
        }
        return finalResponse;
    }


    /**
     * Fungsi ini untuk mencari pemenang dari game hangman.
     */
    public String getGameWinner() {
        HashMap<String, Integer> participantsScore = this.game.getParticipantsScore();
        String gameWinnerID = "";
        int gameWinnerScore = 0;
        for (Map.Entry<String, Integer> entry : participantsScore.entrySet()) {
            if (entry.getValue() > gameWinnerScore) {
                gameWinnerID = entry.getKey();
                gameWinnerScore = entry.getValue();
            }
        }
        String winnerString = "The Winner of this Hangman game is <@" + gameWinnerID + ">";
        return  winnerString;
    }
}
