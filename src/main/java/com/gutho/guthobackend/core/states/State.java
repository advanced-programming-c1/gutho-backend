package com.gutho.guthobackend.core.states;

import com.gutho.guthobackend.core.games.Game;
import com.gutho.guthobackend.core.response.Response;

public interface State {
    Response handleMessage(String userID, String message);

    State nextState();
}
