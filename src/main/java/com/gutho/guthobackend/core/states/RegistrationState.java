package com.gutho.guthobackend.core.states;

import com.gutho.guthobackend.core.games.Game;
import com.gutho.guthobackend.core.games.Hangmen;
import com.gutho.guthobackend.core.response.Response;
import java.util.ArrayList;
import java.util.HashMap;

public class RegistrationState implements State {
    Hangmen game;

    public RegistrationState(Game game) {
        this.game = (Hangmen) game;
    }

    @Override
    public Response handleMessage(String userID, String message) {
        if (game.getParticipantsID().size() > 1 && message.equals("!start")) {
            HashMap<String, Integer> participantsScore = new HashMap<>();

            for (String participant: game.getParticipantsID()) {
                participantsScore.put(participant, 0);
            }
            game.setParticipantsScore(participantsScore);
            game.nextState();
            String messageToStart = "Game will be started! "
                    + "Type \"!startRound\" to start the round!";
            return new Response("GM", userID, messageToStart);
        } else if (game.getParticipantsID().size() == 1 && message.equals("!start")) {
            return new Response("GM", userID, "More than 1 player needed to start the game!");
        }

        ArrayList<String> participantsID = this.game.getParticipantsID();

        if (message.equals("!join")) {
            for (String participantID: participantsID) {
                if (userID.equals(participantID)) {
                    return new Response("GM", userID, "You have already joined the game!");
                }
            }

            game.addParticipant(userID);

            return new Response("GM", userID, "You successfully joined the game! Type \"!start\" "
                    + "to start the game!");
        }

        return new Response("GM", userID, "");
    }

    @Override
    public State nextState() {
        return new ChooseWordState(game);
    }
}
