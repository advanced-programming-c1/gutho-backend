package com.gutho.guthobackend.core.games;

import com.gutho.guthobackend.core.response.Response;
import com.gutho.guthobackend.core.states.State;
import java.util.ArrayList;

public interface Game {
    Response handleMessage(String userID, String message);

    void nextState();

    ArrayList<String> getParticipantsID();

    void addParticipant(String userID);

    String getGameID();

    State getState();
}
