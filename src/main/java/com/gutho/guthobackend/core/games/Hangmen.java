package com.gutho.guthobackend.core.games;

import com.gutho.guthobackend.core.response.Response;
import com.gutho.guthobackend.core.states.RegistrationState;
import com.gutho.guthobackend.core.states.State;
import java.util.ArrayList;
import java.util.HashMap;

public class Hangmen implements Game {
    private State state;
    private String gameID;
    private int roundNo;
    private ArrayList<String> participantsID;
    private HashMap<String, Integer> participantsScore;
    private String currentWord;
    private Boolean[] guessedChar;
    private String choosenID;

    /**
     * Constructor for Hangmen class.
     * @param channelID The channel ID of the message
     */
    public Hangmen(String channelID) {
        this.gameID = channelID;
        this.state = new RegistrationState(this);
        this.participantsID = new ArrayList<>();
        this.participantsScore = new HashMap<>();
    }

    @Override
    public Response handleMessage(String userID, String message) {
        Response response = this.state.handleMessage(userID, message);
        return response;
    }

    @Override
    public void nextState() {
        this.state = this.state.nextState();
    }

    @Override
    public ArrayList<String> getParticipantsID() {
        return participantsID;
    }

    @Override
    public void addParticipant(String userID) {
        this.participantsID.add(userID);
    }

    @Override
    public String getGameID() {
        return this.gameID;
    }

    public String getCurrentWord() {
        return currentWord;
    }

    public void setCurrentWord(String currentWord) {
        this.currentWord = currentWord;
        this.guessedChar = new Boolean[currentWord.length()];
    }

    public Boolean[] getGuessedChar() {
        return guessedChar;
    }

    public void setGuessedChar(Boolean[] guessedChar) {
        this.guessedChar = guessedChar;
    }

    public int getRoundNo() {
        return roundNo;
    }

    public void setRoundNo(int roundNo) {
        this.roundNo = roundNo;
    }

    public String getChoosenID() {
        return choosenID;
    }

    public HashMap<String, Integer> getParticipantsScore() {
        return participantsScore;
    }

    public void setParticipantsScore(HashMap<String, Integer> participantsScore) {
        this.participantsScore = participantsScore;
    }

    public int getAndIncreaseRoundNo() {
        return roundNo++;
    }

    public void setChoosenID(String choosenID) {
        this.choosenID = choosenID;
    }

    public State getState() {
        return state;
    }
}
