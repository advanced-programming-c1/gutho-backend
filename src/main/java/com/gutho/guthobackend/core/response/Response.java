package com.gutho.guthobackend.core.response;

public class Response {
    String responseType;
    String userID;
    String message;

    /**
     * Constructor for Response.
     * @param responseType depends from JSON
     * @param userID ID from user that send the message
     * @param message content of the message
     */
    public Response(String responseType, String userID, String message) {
        this.responseType = responseType;
        this.userID = userID;
        this.message = message;
    }

    public String getResponseType() {
        return responseType;
    }

    public String getUserID() {
        return userID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
