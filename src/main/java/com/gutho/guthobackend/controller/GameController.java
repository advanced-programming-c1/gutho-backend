package com.gutho.guthobackend.controller;

import com.gutho.guthobackend.core.request.Request;
import com.gutho.guthobackend.core.response.Response;
import com.gutho.guthobackend.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/api/v1")
public class GameController {
    @Autowired
    private GameService gameService;

    /**
     * Function to handle process message request.
     * @param request Request from client.
     * @return Response from the system.
     */
    @PostMapping(path = "/processMessage", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity processMessage(@RequestBody Request request) {
        Response response = gameService.handleMessage(request.getChannelID(),
                request.getUserID(), request.getMessage());
        return ResponseEntity.ok(response);
    }
}
