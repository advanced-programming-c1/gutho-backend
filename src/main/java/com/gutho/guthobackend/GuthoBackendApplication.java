package com.gutho.guthobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuthoBackendApplication {
    public static void main(String[] args) {
        SpringApplication.run(GuthoBackendApplication.class, args);
    }
}
