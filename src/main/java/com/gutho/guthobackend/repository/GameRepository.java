package com.gutho.guthobackend.repository;

import com.gutho.guthobackend.core.games.Game;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class GameRepository {
    private List<Game> list;

    public GameRepository() {
        this.list = new ArrayList<>();
    }

    /**
     * Find Game in Repository based on ID.
     * @param channelID ID of the Game
     * @return Game
     */
    public Game getGameById(String channelID) {
        for (Game game: list) {
            if (game.getGameID().equals(channelID)) {
                return game;
            }
        }

        return null;
    }

    public List<Game> getList() {
        return list;
    }

    public Game add(Game game) {
        list.add(game);
        return game;
    }

    public void delete(Game game) {
        list.remove(game);
    }
}
